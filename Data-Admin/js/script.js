(function() {
    if (localStorage.getItem('token') == undefined) {
        window.location.href = '/index.html'
    }
    var BASE_URL = 'http://api.brewfer.com/';
    //var BASE_URL = 'http://localbrewfer.com/';
    var loaderTable;
    var geoTimer;
    $(document).ready(function() {

        // Setup token sending
        $.ajaxSetup({
            data: {
                token: localStorage.getItem('token')
            }
        });

        // Logout
        $('#logout').click(function() {
            localStorage.removeItem('token');
            window.location.href = '/index.html';
        });


        // Populate City Dropdown

        var options = $("#option");
        $.get(BASE_URL + 'api/v1/loader/city-list', function(result) {
            $.each(result, function(key, val) {
                options.append(new Option(val.cyname, val.cyid));
            });
        });


        loaderTable = $('#dataTable').DataTable({
            ajax: {
                "url": BASE_URL + 'api/v1/loader-stats',
                "dataSrc": ''
            },
            "columns": [
                { "data": "number_of_records", title: "Total Records"},
                { "data": "records_inserted", title: "Inserted"},
                { "data": "records_with_errors", title: "Errors" },
                { "data": "file_name", title: "File Name"},
                { "data": "city", title: "City" },
                { "data": "message", title: "Message"},
                { "data": "status", title: "Status" },
                { "data": "created_at", title: "Created At" },
                {
                    "targets": -1,
                    "data": null,
                    "title": "More",
                    "defaultContent": "<button class='pure-button pure-button-primary'>Details</button>"
                }
            ]
        });


        $('#dataTable tbody').on( 'click', 'button', function () {
            var data = loaderTable.row( $(this).parents('tr') ).data();
            showModal(data.id);
        } );

        $('#form-upload').submit(function(event) {
            event.preventDefault();
            var form = $(this);
            var file = $('#bulk_upload');
            if (file.val() != '') {
                $('#fileName').val(file.prop("files")[0]['name']);
            } else {
                flashMessage('Please attach file before uploading.');
                return;
            }
            if ($('#option').val() == null) {
                flashMessage('Please Select City');
                return;
            }

            var fd = new FormData(document.getElementById("form-upload"));
            fd.append("label", "WEBUPLOAD");
            $.ajax({
                url: BASE_URL + 'api/v1/upload',
                type: "POST",
                data: fd,
                enctype: 'multipart/form-data',
                processData: false,  // tell jQuery not to process the data
                contentType: false   // tell jQuery not to set contentType
            }).done(function( data ) {
                flashMessage(data.message);
            }).fail(function(data) {
                flashMessage('Error in queuing Job: ' + JSON.stringify(data));
            });
        });

        setInterval( function () {
            loaderTable.ajax.reload();
        }, 5000);

        function showModal(id) {
            var source   = $("#details-modal-template").html();
            var template = Handlebars.compile(source);
            $.ajax({
               url: BASE_URL + 'api/v1/loader-stats/' + id,
               method: 'GET',
               success: function(data) {
                   var context = {
                       title: data.file_name,
                       errors: data.error_list,
                       created: data.created_at,
                       status: data.status,
                       message: data.message,
                       records: data.number_of_records || 0,
                       inserted: data.records_inserted || 0,
                       errorNumber: data.records_with_errors || 0,
                       success: (data.status == 'Completed'),
                       showDelete: (data.records_inserted > 0 && data.status != 'Deleted'),
                       city: data.city,
                       id: data.id
                   };
                   console.log(data);
                   var html    = template(context);
                   var modal = $("#details-modal");
                   modal.html(html);
                   var remodal = modal.remodal();
                   remodal.open();
                   $("#deleteRecords").off('click').on('click', function() {
                       var confirm = window.confirm('Are you sure that you want to delete records for this batch?')
                       if (confirm) {
                           $.post(BASE_URL + 'api/v1/loader/delete', {loader_id: id}, function(data) {
                           }).always(function() {
                               remodal.close();
                           })
                       }
                   });
               },
               error: function() {
                   alert('Failed to reach server');
               }
            });
        }

        function flashMessage(message) {
            var status = $("#status");
            status.html(message);
            status.fadeIn();
            setTimeout(function() {
                status.fadeOut();
            }, 3000)
        }

        $('#geo-coords').on( 'click', function () {
            var source   = $("#geo-modal-template").html();
            var template = Handlebars.compile(source);
            $.ajax({
                url: BASE_URL + 'api/v1/geocoordinates',
                method: 'GET',
                success: function(data) {
                    var context = {
                        title: 'Fetch Geo Co-ordinates',
                        records_to_be_updated: data.records_to_update,
                        records_updated: data.records_updated,
                        records_cannot_update: data.records_cannot_update
                    };
                    console.log(data);
                    getGeoCountStatus(true);
                    var html    = template(context);
                    var modal = $("#details-modal");
                    modal.html(html);
                    var remodal = modal.remodal();
                    remodal.open();
                    $('#fetch-geo').on( 'click', function () {
                        $.ajax({
                            url: BASE_URL + 'api/v1/geocoordinates',
                            method: 'POST',
                            success: function (responseData) {
                                console.log(responseData.message);
                                $('#geo-message').html(responseData.message);
                            },
                            error: function(){
                                alert('Fail to reach server in up');
                            }
                        });
                    });
                },
                error: function() {
                    alert('Failed to reach server up');
                }
            });
        });
        $("#geoClose").on( 'click', function () {
            alert("test");
            getGeoCountStatus(false);
        });
    });
    function getGeoCountStatus(flag) {
        if(!geoTimer){
            geoTimer = setInterval(function () {
                $.ajax({
                    url: BASE_URL + 'api/v1/geocoordinates',
                    method: 'GET',
                    success: function (data) {
                        $('#record_to_updated').html(data.records_to_update);
                        $('#record_updated').html(data.records_updated);
                        $('#record_cannot_updated').html(data.records_cannot_update);
                        console.log(data);
                    },
                    error: function () {
                        alert('Failed to reach server');
                    }
                });
            }, 3000);
        }
        if(!flag) {
            clearInterval(geoTimer);
        }
    }

})();