/**
 * Created by karthik on 10/20/15.
 */

//var BASE_URL = 'http://localbrewfer.com/';
var BASE_URL = 'http://api.brewfer.com/';

if (localStorage.getItem('token') != undefined) {
    window.location.href = '/dashboard.html'
}

$("#form").submit(function(e) {
    e.preventDefault();
    var password = $("#password").val();
    var username = $("#username").val();
    if (username == '' || password == '') {
        alert('Please check your input');
        return;
    }
    $.ajax({
        url: BASE_URL + 'api/v1/admin-login',
        type: "POST",
        data:{
            username:username,
            password:password
        }
    }).done(function( data ) {
        console.log(data);
        if (data.status == 'error') {
            alert('Invalid Credentials');
        } else if (data.status = 'success') {
            localStorage.setItem('token',  data.data);
            window.location.href = '/home.html'
        }
    }).fail(function() {
        alert('Cannot connect to server');
    });
});

