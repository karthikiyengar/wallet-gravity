<?php
/**
 * Created by PhpStorm.
 * User: karthik
 * Date: 9/18/15
 * Time: 12:28 AM
 */

namespace App\Classes;

use Exception;

class DataExtractor {
    private $daysOfWeek;

    function __construct() {
        $this->daysOfWeek = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
    }
    public function extractBankData($string) {
        $outputData = [];
        try {
            $banksWithOffers = explode(',', $string);
            foreach ($banksWithOffers as $record) {
                $record = explode('-', $record);
                /* Key value pair for banks and offers */
                $outputData[trim($record[0])] = trim($record[1]);
            }
        } catch (Exception $e) {
            return ['error' => 'Invalid Format'];
        }
        return $outputData;
    }

    public function extractPaymentData($row) {
        $payments = [];
        $this->parseBoolean($row->accepts_cash) ? $payments[] = 'Cash': null;
        $this->parseBoolean($row->accepts_cards) ? $payments[] = 'Cards' : null;
        $this->parseBoolean($row->accepts_meal_coupons) ? $payments[] = 'Meal Coupons' : null;
        return $payments = implode(', ', $payments);
    }

    public function extractPhoneNumbers($string) {
        $outputData = [];
        try {
            $contactNumbers = explode(',', $string);
            foreach ($contactNumbers as $number) {
                $number = trim($number);
                /* Check if number begins with a "+" and has at least 8 characters */
                if (preg_match('/\+[0-9]*$/', $number) && strlen($number) >= 8) {
                    $outputData[] = $number;
                } else {
                    throw new Exception();
                }
            }
            return implode(',', $outputData);
        } catch (Exception $e) {
            return ['error' => 'Invalid Format'];
        }
    }

    public function extractCuisineData($string) {
        $outputData = [];
        try {
            $cuisines = explode(',', $string);
            foreach ($cuisines as $cuisine) {
                $cuisine = trim($cuisine);
                /* Check if Cuisine only has spaces and alphabets */
                if (preg_match('/^[A-Za-z\s]*$/', $cuisine)) {
                    /* For title casing */
                    $outputData[] = ucwords(strtolower($cuisine));
                } else {
                    throw new Exception();
                }
            }
            return $outputData;
        } catch (Exception $e) {
            return ['error' => 'Invalid Format'];
        }
    }

    public function extractTimingData($string)
    {
        $outputData = [];
        try {
            $timings = explode(',', $string);
            $outputData = [];
            foreach($timings as $timing) {
                $timing = trim($timing);

                /* Check if timings are in Format 2 (Explicit for all days) */
                if (isset(date_parse(substr($timing,0,3))["relative"]["weekday"])) {
                    $day = ucfirst(substr($timing,0,3)); #"Sun"
                    $period = trim(substr($timing, 3)); #"12 PM to 3 PM and 7 PM to 11 PM"
                    $validatedPeriod = $this->parseTimePeriodString($period);
                    if (!isset($outputData[$day]) && !isset($validatedPeriod["error"])) {
                        $outputData[$day] = $validatedPeriod;
                    } else {
                        throw new Exception();
                    }
                }
                /* For Format 1 (One string for all days) */
                else {
                    $validatedPeriod = $this->parseTimePeriodString($string);
                    if (!isset($validatedPeriod["error"])) {
                        foreach($this->daysOfWeek as $day) {
                            $outputData[$day] = $validatedPeriod;
                        }
                    } else {
                        throw new Exception($validatedPeriod["error"]);
                    }
                }
            }
            return $outputData;
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    /***
     * Accepts a time period and validates/standardizes it.
     * E.g: "7:30 A.M to 10:30 AM" will return "07:30 AM to 10:30 AM"
     * @param $string
     * @return array|string
     */
    public function parseTimePeriodString($string) {
        try {
            if (trim(strtolower($string)) == '24 hours') {
                return '24 Hours';
            }
            $period = explode('and', $string); #["12 PM to 3 PM","7 PM to 11 PM"]
            foreach ($period as $times) {
                $unit = explode('to', $times); #["12 PM", "3 PM"]

                if (count($unit) > 2) {
                    throw new Exception();
                }
                $startTime = strtotime($unit[0]);
                $endTime = strtotime($unit[1]);

                if (!$startTime || !$endTime) {
                    throw new Exception('Unable to parse time');
                }

                $unitArray[] = date('h:i A', $startTime) . " to " . date('h:i A', $endTime);
            }
            return implode(' and ', $unitArray);
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (empty($message)) {
                $message = "Please check format for timings";
            }
            return ['error' => $message];
        }
    }

    /***
     * Return true or false depending on Yes/No
     * @param $string
     * @return bool
     */
    public function parseBoolean($string) {
        if ($string == 'Yes') {
            return true;
        } else if ($string == 'No') {
            return false;
        } else if (empty(trim($string))) {
            return false;
        }
    }

    /***
     * Checks whether the time period is valid or not (start time < end time)
     * @param $string
     */
    public function checkIfValidTimePeriod($string) {
        /* Get if start time is less than end time */
        $period = explode('and', $string); #["12 PM to 3 PM","7 PM to 11 PM"]
        foreach ($period as $times) {
            $unit = explode('to', $times); #["12 PM", "3 PM"]

            $startTime = strtotime($unit[0]);
            $endTime = strtotime($unit[1]);

            if ($startTime <= $endTime) {
                return true;
            }
        }
    }
}