<?php namespace App\Classes;

class Constants
{
    const UNAUTHORIZED = 'These credentials do not match our records.';
    const RESOURCEUNAUTHORIZED = 'This resource is not accessible to you.';
    const RESOURCEDOESNOTEXIST = 'Resource does not exist.';
    const LOGGEDOUT = 'You have been logged out successfully';
    const MESSAGESENT = "Response from Server: ";
    const ALREADYREGISTERED = "Your have already registered with this email id.";
}