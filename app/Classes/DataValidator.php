<?php
/**
 * Created by PhpStorm.
 * User: karthik
 * Date: 9/17/15
 * Time: 11:21 AM
 */

namespace App\Classes;

use App\Models\Bank;
use App\Models\Client;
use App\Models\Cuisine;
use Illuminate\Support\Facades\Config;

class DataValidator {

    private $dataExtractor, $numericFields;

    function __construct() {
        $this->dataExtractor = new DataExtractor();
        $this->mandatoryFields = Config::get('loader-config.mandatoryFields');
        $this->bitFields = Config::get('loader-config.bitFields');
        $this->numericFields = Config::get('loader-config.numericFields');
    }


    public function validateHeadersForRow($row) {
        $fileHeaders = Config::get('loader-config.fileHeaders');
        $diff = array_diff($row->keys()->toArray(), $fileHeaders);
        if (empty($diff)) {
            return ["success" => "File matches"];
        } else {
            return $diff;
        }
    }

    /***
     * Applies data validation for row and returns an array of errors
     * @param $row
     * @return array
     */
    public function applyValidationToRow($row) {
        $errorList = [];

        /* Check mandatory fields which are not present */
        $errorFields = [];
        foreach($this->mandatoryFields as $field) {
            if (empty($row->$field)) {
                $errorFields[] = $field;
            }
        }

        if (count($errorFields) > 0) {
            $errorList[] = 'Mandatory field(s) missing: ' . implode($errorFields,', ');
        }

        /* Check for bit fields */
        $errorFields = [];
        foreach($this->bitFields as $field) {
            if (!empty($row->$field) && !in_array($row->$field, ['Yes', 'No'])) {
                $errorFields[] = $field;
            }
        }
        if (count($errorFields) > 0) {
            $errorList[] = 'Bad values for field(s). Should be Yes/No: ' . implode($errorFields, ', ');
        }


        /* Check for numeric fields */
        $errorFields = [];
        foreach ($this->numericFields as $field) {
            if (!empty($row->$field) && !is_numeric($row->$field)) {
                $errorFields[] = $field;
            }
        }
        if (count($errorFields) > 0) {
            $errorList[] = 'Non-Numeric values found: ' . implode($errorFields, ', ');
        }

        /* Check for Duplicates */
        $count = Client::where('Restaurant_Name', $row->restaurant_name)
            ->where('Area', $row->area)
            ->where('Address', $row->address)
            ->count();
        if ($count > 0) {
            $errorList[] = 'Record already exists in database';
        }

        /* Check if offers can be parsed */

        if (!empty($row->offers)) {
            $dataCheck = $this->dataExtractor->extractBankData($row->offers);
            if (isset($dataCheck['error'])) {
                $errorList[] = 'Offers cannot be parsed';
            }
        }

        /* Check if phone numbers are valid */
        if (!empty($row->contact_numbers)) {
            $dataCheck = $this->dataExtractor->extractPhoneNumbers($row->contact_numbers);
            if (isset($dataCheck['error'])) {
                $errorList[] = 'Invalid Phone Numbers';
            }
        }

        /* Check if email Address is valid */
        if (!empty($row->email) && !filter_var($row->email, FILTER_VALIDATE_EMAIL)) {
            $errorList[] = 'Email ID Invalid';
        };

        /* Check if cuisines can be parsed */
        if (!empty($row->cuisines)) {
            $dataCheck = $this->dataExtractor->extractCuisineData($row->cuisine);
            if (isset($dataCheck['error'])) {
                $errorList[] = 'Offers string cannot be parsed';
            }
        }

        /* Check if timings can be parsed */
        if (!empty($row->timings)) {
            $dataCheck = $this->dataExtractor->extractTimingData($row->timings);
            if (isset($dataCheck['error'])) {
                $errorList[] = $dataCheck['error'];
            }
        }
        return $errorList;
    }
}