<?php
namespace App\Classes;

use App\Classes\Constants;
use Config;
use Teepluss\Restable\Facades\Restable;
use Unirest\Request;

class Commons
{

    /**
     * Generic SMS sending method.
     * @param $receivers
     * @param $message
     * @return string
     */
    public static function sendSms($receivers, $message)
    {
        $url      = Config::get('services.sms.url');
        $username = Config::get('services.sms.username');
        $password = Config::get('services.sms.password');
        $sender   = Config::get('services.sms.sender');

        $receiversString = rtrim(implode(',', $receivers), ',');
        $message         = urlencode($message);
        if (strlen($message) > 0 && count($receivers) > 0) {
            Request::verifyPeer(false);
            $uri      = $url . "?username=" . $username . "&password=" . $password . "&type=TEXT&sender=" . $sender . "&mobile=" . $receiversString . "&message=" . $message;
            $response = Request::get($uri);
            return Restable::success(Constants::MESSAGESENT . $response->body)->render();
        } else {
            return Restable::bad()->render();
        }
    }
}