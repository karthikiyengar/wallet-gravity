<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model {
	public $timestamps = false;
    protected $table = 'fav_rest';
    protected $primaryKey = 'fid';
    protected $fillable = array('client_id', 'uid');

    public function client(){
        return $this->belongsTo('\App\Models\Client', 'client_id');
    }

    public function user(){
        return $this->belongsTo('App\User','uid');
    }

}
