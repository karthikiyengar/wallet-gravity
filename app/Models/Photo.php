<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model {

    public $timestamps = false;
    public $primaryKey = 'id';
    public $table = 'photos';

    public function client(){
        return $this->belongsTo('App\Models\Client','client_id');
    }

}
