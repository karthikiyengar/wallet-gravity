<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {

	public $timestamps = false;
    protected $table = 'clients';
    protected $primaryKey = 'clid';
    protected $hidden = array('password','login','timing');
    protected $casts = [
        'cost' => 'integer'
    ];

    public function review() {
        return $this->hasMany('\App\Models\Review', 'client_id');
    }

    public function cuisines(){
        return $this->hasMany('\App\Models\Cuisine', 'client_id');
    }

    public function city() {
        return $this->belongsTo('\App\Models\City', 'cityid');
    }

    public function rating() {
        return $this->hasMany('\App\Models\Rating', 'client_id');
    }

    public function favourite(){
        return $this->hasMany('\App\Models\Favourite', 'client_id');
    }

    public function banks() {
        return $this->join('bank_client', 'clients.clid', '=', 'bank_client.')->get();
    }

    public function menus(){
        return $this->hasMany('\App\Models\Menu', 'client_id');
    }

    public function photos(){
        return $this->hasMany('\App\Models\Photo', 'client_id');
    }

    public function timings() {
        return $this->hasMany('\App\Models\Timings', 'client_id');
    }

    public function discounts() {
        return $this->hasMany('\App\Models\Discount', 'client_id');
    }

    public function banners(){
        return $this->hasMany('\App\Models\Banner', 'client_id');
    }
}
