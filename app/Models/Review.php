<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model {

    public $timestamps = false;
    protected $table = 'review';
    protected $primaryKey = 'rid';
    protected $fillable = array('client_id', 'uid', 'review');

    public function user() {
        return $this->belongsTo('App\User', 'uid');
    }

    public function client() {
        return $this->belongsTo('App\Models\Client', 'client_id');
    }

    public function likes() {
        return $this->hasMany('App\Models\LikeForReview', 'reviewid');
    }

    public function comments(){
        return $this->hasMany('App\Models\Comment','reviewid');
    }
}

