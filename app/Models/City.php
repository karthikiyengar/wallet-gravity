<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

    protected $table = 'city';
	public $timestamps = false;
    protected $primaryKey = 'cyid';

    public function country()
    {
        return $this->belongsTo('\App\Models\Country', 'cnid');
    }

    public function state()
    {
        return $this->belongsTo('\App\Models\State', 'steid');
    }

    public function cuisine(){
        return $this->hasMany('\App\Models\Cuisine','cityid','cyid');
    }

    public function clients() {
        return $this->hasMany('\App\Models\Client', 'cityid');
    }

    public function banks(){
        return $this->hasMany('\App\Models\Bank','cityid');
    }
}
