<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model {

    public $timestamps = false;
    public $primaryKey = 'id';
    public $table = 'banner';

    public function client(){
        return $this->belongsTo('App\Models\Client','client_id');
    }

}
