<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

	public $timestamps = false;
    protected $table = 'comments_table';
    protected $primaryKey = 'id';

    public function likes(){
        return $this->hasMany('App\Models\LikeForComment','cid');
    }

    public function user(){
        return $this->belongsTo('App\User','uid');
    }

    public function review(){
        return $this->belongsTo('App\Models\Review','reviewid');
    }
}
