<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LikeForReview extends Model {

    public $timestamps = false;
    protected $table = 'likes_table';
    protected $primaryKey = 'reviewid';
    protected $fillable = array('reviewid');

    public function review() {
        return $this->belongsTo('App\Models\Review','reviewid','rid');
    }

    public function user() {
        return $this->belongsTo('App\User','uid','uid');
    }
}
