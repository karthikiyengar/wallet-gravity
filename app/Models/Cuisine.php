<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cuisine extends Model {

	public $timestamps = false;
    protected $table = 'cousines';

    public function clients() {
        return $this->belongsTo('App\Models\Client', 'client_id', 'clid');
    }

    public function city(){
        return $this->BelongTo('App\Models\City','cityid','cyid');
    }
}
