<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LiveOffers extends Model {

    public $table = 'liveoffers';
    protected $timestamp = 'false';

    public function clients(){
    	return $this->belongsTo('\App\Models\Client', 'cid');
    }
}
