<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Security extends Model {

    public $timestamps = false;
    protected $table = 'security';
    protected $primaryKey = 'sid';
    protected $fillable = array('sid');

}
