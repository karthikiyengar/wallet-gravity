<?php
/**
 * Created by PhpStorm.
 * User: Prasad-PC
 * Date: 9/23/2015
 * Time: 9:43 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model {
    public $timestamps = false;
    protected $table = 'fpwdsec';
    protected $primaryKey = 'id';
    protected $fillable = array('email_id', 'token', 'stime');
} 