<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model {
	public $timestamps = false;
    protected $table = 'ratings';
    protected $primaryKey = 'id';
    protected $fillable = ['client_id', 'uid', 'rating'];
    public function client() {
        return $this->belongsTo('\App\Models\Client', 'client_id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'uid');
    }
}