<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {
    public $timestamps = false;
    public $primaryKey = 'id';
    public $table = 'menus';

    public function client(){
        return $this->belongsTo('App\Models\Client','client_id');
    }

}
