<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LikeForComment extends Model {

	public $timestamps = false;
    protected $table = 'comment_likes';
    protected $primaryKey = 'id';
    protected $fillable = array('cid');

    public function user() {
        return $this->belongsTo('App\User','uid');
    }

    public function comment(){
        return $this->belongsTo('App\Models\Comment','cid','id');
    }
}
