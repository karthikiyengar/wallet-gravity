<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model {
    public $timestamps = false;
    protected $table = 'discounts';

    public function client(){
        return $this->belongsTo('App\Models\Client','client_id');
    }

    public function bank(){
        return $this->hasOne('App\Models\Bank','bank_id', 'bank_id');
    }
}
