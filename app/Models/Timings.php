<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timings extends Model {

    public $timestamps = false;
    protected $table = 'timings';
    protected $hidden = array('client_id','cityid');

    public function client() {
        return $this->belongsTo('App\Models\Client', 'client_id');
    }
}
