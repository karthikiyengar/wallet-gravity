<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HappyHours extends Model {

	public $table = 'happyhrs';
    protected $timestamp = 'false';

    public function clients(){
    	return $this->belongsTo('\App\Models\Client', 'cid');
    }
}
