<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::GET('/', function() {
    return 'You have arrived!';
});

/* Protected API Routes: Needs Authorization Token */
Route::GROUP(['middleware' => ['jwt.auth']], function() {

    /**
     * Routes for Reviews
     */

    Route::PUT('api/v1/review', 'ReviewController@store');
    Route::DELETE('api/v1/review/{review}', 'ReviewController@destroy');

    /**
     * Routes for Comments
     */

    Route::POST('api/v1/comment', 'CommentController@store');
    Route::PUT('api/v1/comment', 'CommentController@update');
    Route::DELETE('api/v1/comment', 'CommentController@destroy');

    /**
     * Routes for Like Comment
     */
    Route::PUT('api/v1/like-comment', 'LikeCommentController@store');
    Route::DELETE('api/v1/like-comment', 'LikeCommentController@destroy');


    /**
     * Routes for Like Review
     */
    Route::PUT('api/v1/like-review', 'LikeReviewController@store');
    Route::DELETE('api/v1/like-review/{likereview}', 'LikeReviewController@destroy');


    /**
     * Routes for City Chooser
     */

    Route::POST('api/v1/set-city', 'CityController@setCity');

    /**
     * Routes for Happy Hours and Live Offers
     */
    Route::GET('api/v1/happy-hours','DiscountController@showHappyHours');
    Route::GET('api/v1/live-offers','DiscountController@showLiveOffers');

    /*
     * Routes from Client Profile
     */
    Route::POST('api/v1/rating', 'ClientProfileController@storeRating');
    Route::POST('api/v1/favourite', 'ClientProfileController@storeFavourite');

    /*
    * Routes for User favourite list
    */
    Route::GET('api/v1/favourite/list', 'FavouriteController@favouriteList');

    /*
    * Routes for Feedback from app users about app.
    */
    Route::POST('api/v1/feedback', 'feedbackController@feedback');

    /**
     * Routes for Profile
     */
    Route::POST('api/v1/set-profile', 'Auth\AuthController@setProfile');
    Route::get('api/v1/get-profile', 'Auth\AuthController@getProfile');
});

/**
 * Authentication Routes
 */
Route::POST('auth/login', 'Auth\AuthController@postLogin');
Route::POST('auth/login/facebook', 'Auth\AuthController@postLoginFacebook');
Route::POST('auth/login/gplus', 'Auth\AuthController@postLoginGplus');
Route::POST('auth/login/forgot-password', 'Auth\AuthController@forgotPassword');
Route::GET('auth/logout', 'Auth\AuthController@getLogout');
Route::GET('auth/settings', 'Auth\AuthController@getSettings');

/*
 * Registration Routes
 */
Route::POST('auth/register', 'Auth\AuthController@register');

Route::controllers([
    'password' => 'Auth\PasswordController',
]);

Route::GROUP(['middleware' => ['admin.auth']], function() {
    Route::POST('api/v1/upload', 'LoadingTaskController@readFile');
    Route::GET('api/v1/loader-stats', 'LoadingTaskController@getStats');
    Route::GET('api/v1/loader-stats/{id}', 'LoadingTaskController@getStats');
    Route::GET('api/v1/loader/city-list', 'LoadingTaskController@getCity');
    Route::POST('api/v1/loader/delete', 'LoadingTaskController@deleteBatch');
    Route::POST('api/v1/set-user-bank', 'FavouriteController@setUserBank');

    /**
     * Fetch GEO coordinates  routes
     */
    Route::GET('api/v1/geocoordinates', 'GeoController@getFetchCoordinates');
    Route::POST('api/v1/geocoordinates', 'GeoController@fetchCoordinates');
});

/***
 * Admin Login
 */
Route::POST('api/v1/admin-login', 'Auth\AuthController@adminLogin');


/**
 * Open-Routes for Search
 */
Route::GET('api/v1/search','SearchController@search');
Route::GET('api/v1/autocomplete','SearchController@autocomplete');
Route::GET('api/v1/get-cities', 'CityController@getCities');
Route::GET('api/v1/get-city-list', 'CityController@getCityList');
Route::GET('api/v1/search/gps', 'SearchController@searchGps');
/**
 * Routes for SMS
 */
Route::POST('api/v1/sms-details', 'SmsController@sendClientDetails');
Route::GET('api/v1/client', 'ClientProfileController@show');

/**
 * Open-Routes for Review
 */
Route::GET('api/v1/review', 'ReviewController@show');


