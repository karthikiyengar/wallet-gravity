<?php namespace App\Http\Middleware;

use Closure;
use Teepluss\Restable\Facades\Restable;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdminAuth {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        $token = $request->input('token');

        if (empty($token)) {
            return Restable::unauthorized()->render();
        } else {
            try {
                $parsedToken = JWTAuth::parseToken($token)->getPayload()->get('sub');
            } catch(JWTException $e) {
                return Restable::unauthorized()->render();
            }

            if (!empty($parsedToken)) {
                return $next($request);
            } else {
                return Restable::unauthorized()->render();
            }
        }
	}

}
