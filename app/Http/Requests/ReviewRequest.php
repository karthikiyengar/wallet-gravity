<?php namespace App\Http\Requests;

class ReviewRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
        #TODO Use this to check whether the is logged in, or owns a review.
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'review' => 'required|max:500',
		];
	}

}
