<?php namespace App\Http\Controllers;

use App\Classes\Constants;
use App\Http\Requests;
use App\Models\Comment;
use App\Models\LikeForComment;
use Carbon\Carbon;
use HttpResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use SebastianBergmann\GlobalState\RuntimeException;
use Teepluss\Restable\Facades\Restable;

class LikeCommentController extends Controller {



	/**
	 * Get the userId and CommentId. Check if the Like already exists. If yes then update else insert like.
     * Allow user to like only if Comment exists else throw exception.
     * By - Bala
     * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $user = Auth::user();
        $commentId = $request->input('comment_id');
        $likeComment = LikeForComment::firstOrNew(array(
            'uid' => $user->uid,
            'cid' => $commentId,
        ));
        try {
            Comment::findOrFail($commentId);
            $likeComment->uid = $user->uid;
            $likeComment->status = $request->input('status');
            $likeComment->time1 = Carbon::now();
            $likeComment->save();
            return Restable::created($likeComment)->render();
        }
        catch(ModelNotFoundException $e){
            return Restable::missing(Constants::RESOURCEDOESNOTEXIST)->render();
        }
	}

	/**
     * Get the userId and CommentId. Check if the Like already exists. If yes then update else insert like.
     * Ensure that the authorized user is only allowed to delete the like.
     * By - Bala
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
        try {
            $user = Auth::user();
            $id = $request->input('comment_id');
            $likeComment = LikeForComment::findOrFail($id);
            if ($likeComment->user()->first() == $user) {
                $likeComment->delete();
                return Restable::deleted()->render();
            }
            else {
                throw new RuntimeException();
            }
        }
        catch (ModelNotFoundException $e) {
            return Restable::missing(Constants::RESOURCEDOESNOTEXIST)->render();
        }
        catch (RuntimeException $e) {
            return Restable::unauthorized(Constants::RESOURCEUNAUTHORIZED)->render();
        }
	}

}
