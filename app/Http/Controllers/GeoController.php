<?php namespace App\Http\Controllers;

use App\Commands\CalculateGeoCoords;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\Request;
use Queue;

class GeoController extends Controller {

    public function getFetchCoordinates()
    {
        $data = array();
        $clients = Client::all();
        if (!empty($clients)) {
            #If new then get no.of records to be update is_process = 0  and with no coordinates of that loader_task_id

            $records_to_update               = $clients->filter(function($item)
            {
                return ($item->is_geo_processed != 1);
            });
            #dd($records_to_update);
            $data['records_to_update'] = $records_to_update->count();

            #If new then get no.of records to be update is_process = 1 of that loader_task_id

            $records_updated                = $clients->filter(function($item)
            {
                return ($item->is_geo_processed == 1 && $item->latitude != NULL && $item->longitude != NULL);
            });
            $data['records_updated']  = $records_updated->count();

            $records_cannot_update               = $clients->filter(function($item)
            {
                return ($item->is_geo_processed == 1 && $item->latitude == NULL && $item->longitude == NULL);
            });
            $data['records_cannot_update'] = $records_cannot_update->count();
            return $data;
        }
        //return view("Geo.getGeoCoords")->with('data', $data);
    }

    public function fetchCoordinates(Request $request)
    {
        Queue::push(new CalculateGeoCoords());
        return ["status" => "success", "message" => "Job queued successfully"];
    }
}
