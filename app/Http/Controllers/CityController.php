<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\City;
use App\Models\State;
use App\Models\Country;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Teepluss\Restable\Facades\Restable;

class CityController extends Controller {
    public function setCity(Request $request) {
        $user = Auth::user();

        $inpCity = $request->input('city');
        $inpState = $request->input('state');

        $city = State::where('stname', $inpState)->first()->cities()->where('cyname', $inpCity)->first();
        if (!empty($city) && !empty($user)) {
            $user->cityid = $city->cyid;
            $user->save();
            return Restable::success($user)->render();
        }
        else {
            return Restable::bad()->render();
        }
    }

    public function getCities(Request $request) {
        return Restable::listing(Country::with(array('city' => function($query) {
            $query->whereIn('cyid', Client::distinct()->get(['cityid'])->toArray())->with('state')->orderby('cyname');
        }))->get())->render();
    }
    public function getCityList() {
        return City::whereIn('cyid', Client::distinct()->get(['cityid'])->toArray())->get();
    }
}
