<?php namespace App\Http\Controllers;

use App\Classes\Constants;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Client;
use App\Classes\Commons;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Teepluss\Restable\Facades\Restable;
use Teepluss\Restable\RestableServiceProvider;

class SmsController extends Controller {

    /**
     * Get details for a particular client in SMS
     * @param Request $request
     * @return string
     */
    public function sendClientDetails(Request $request)
    {
        try {
            $id = $request->input('client_id');
            $mobileNumber = $request->input('mobile_number');
            $client = Client::findOrFail($id);
            $message = 'Hi, here are the details for ' . $client->Restaurant_Name . '. Contact: ' . $client->contact . '. Address: ' . $client->address . '. Visit http://www.brewfer.com/search_details.php?id='. $client->clid .' for more details.';
            return Commons::sendSms(array($mobileNumber), $message);
        }
        catch (ModelNotFoundException $e) {
            return Restable::missing(Constants::RESOURCEDOESNOTEXIST)->render();
        }
    }
}
