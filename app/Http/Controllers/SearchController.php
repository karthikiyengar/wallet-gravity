<?php namespace App\Http\Controllers;

use App\Classes\Constants;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Client;
use DB;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Teepluss\Restable\Facades\Restable;

class SearchController extends Controller {

    public function searchGps(Request $request) {
        $latitude = $request->latitude;
        $longitude = $request->longitude;

        if (!is_numeric($latitude) || !is_numeric($longitude)) {
            return 'Invalid Input';
        }

        $latitude = DB::connection()->getPdo()->quote($latitude);
        $longitude = DB::connection()->getPdo()->quote($longitude);

        return DB::select("
                    SELECT   distance,
                   `clid`,
                   `countryid`,
                   `stateid`,
                   `NEARBY`.`cityid`,
                   `Restaurant_Name`,
                   `contact`,
                   `email`,
                   `address`,
                   `area`,
                   `hd`,
                   `dinein`,
                   `svegnn`,
                   `bar`,
                   `st`,
                   `aircond`,
                   `cuisine`,
                   `knownfor`,
                   `order_sugest`,
                   `payment`,
                   `cost`,
                   `latitude`,
                   `longitude`,
                   `avgrating`,
                   Group_concat(Concat(Concat(banks.bank, ' - '), discounts.discount)
                   SEPARATOR
                   ',  ')
                   AS discounts FROM
            (SELECT ( 6371 * Acos (Cos (Radians($latitude)) * Cos(Radians(latitude)) * Cos(
                                                        Radians(longitude) -
                                                        Radians($longitude)) +
                                         Sin
                                           (
                                                  Radians($latitude)) * Sin(Radians(latitude))
                            ) ) AS
                   distance,
                   `clid`,
                   `countryid`,
                   `stateid`,
                   `clients`.`cityid`,
                   `Restaurant_Name`,
                   `contact`,
                   `email`,
                   `address`,
                   `area`,
                   `hd`,
                   `dinein`,
                   `svegnn`,
                   `bar`,
                   `st`,
                   `aircond`,
                   `cuisine`,
                   `knownfor`,
                   `order_sugest`,
                   `payment`,
                   `cost`,
                   `latitude`,
                   `longitude`
            FROM   `clients`
            HAVING distance >= 0 and distance <= 25
            ORDER BY distance ASC LIMIT 30) NEARBY
            LEFT JOIN `discounts`
                  ON `discounts`.`client_id` = `NEARBY`.`clid`
            LEFT JOIN `banks`
                  ON `banks`.`bank_id` = `discounts`.`bank_id`
            LEFT JOIN (SELECT client_id,
                                 Round(Avg(rating), 1) AS avgrating
                          FROM   ratings
                          GROUP  BY client_id) AVERAGE
                      ON `NEARBY`.`clid` = `AVERAGE`.`client_id`
            GROUP  BY `NEARBY`.`clid`
            ORDER BY DISTANCE ASC;
        ");
    }

    public function search(Request $request)
    {
        DB::enableQueryLog();

        # Take inputs from user request
        $inputArray['sort'] = $request->input('sort');
        $inputArray['order'] = $request->input('order');
        $inputArray['bank'] =$request->input('bank');
        $inputArray['cost'] = $request->input('cost');
        $inputArray['cuisine'] = $request->input('cuisine');
        $inputArray['area-client'] = $request->input('area-client');
        $inputArray['area'] = $request->input('area');
        $inputArray['bank-name'] = $request->input('bank-name');

        # Store flags form the input in a separate array
        $flagArray['hd'] = $request->input('hd');
        $flagArray['dinein'] = $request->input('dinein');
        $flagArray['svegnn'] = $request->input('svegnn');
        $flagArray['bar'] = $request->input('bar');
        $flagArray['aircond'] = $request->input('aircond');


        #Create a non empty array and store the values from the input array which are not empty
        $nonEmptyArray = array();
        foreach ($inputArray as $key => $value) {
            if (!empty($value)) {
                $nonEmptyArray[$key] = $value;
            }
        }

        $query = DB::table('clients');
        #Select records from the client table according to the city of the user


        #Left join with the ratings table (Grouped by client_id to get average rating)
        $query = $query->leftJoin(DB::raw('(SELECT client_id,  round(avg(rating), 1) as avgrating FROM ratings GROUP BY client_id) average'),  function($join) {
            $join->on('clid',  '=',  'client_id');
        });

        #Left join with the discounts table
        $query = $query->leftJoin('discounts',  'discounts.client_id',  '=',  'clients.clid');

        #Left join with the banks table
        $query = $query->leftJoin('banks',  'banks.bank_id',  '=',  'discounts.bank_id');



        #Start processing inputs
        if (count($nonEmptyArray) > 0 || count($flagArray > 0)) {
            if (array_key_exists('area-client',  $nonEmptyArray)) {
                $value = $nonEmptyArray['area-client'];
                $query = $query->where(function($query) use ($value) { $query -> where('clients.area', 'like',  "%$value%") -> orWhere('clients.Restaurant_Name', 'like',  "%$value%");});
            }

            if (array_key_exists('cost',  $nonEmptyArray)) {
                $value = $nonEmptyArray['cost'];
                $values = explode(',', $value);
                $query = $query->whereRaw("clients.cost BETWEEN " . $values[0] . " AND " . $values[1]);
            }
            foreach($flagArray as $key => $value) {
                if (!empty($value)) {
                    $nonEmptyArray[$key] = $value;
                    $query = $query->where("clients.$key",  '=',  $value);
                }
            }
            if (array_key_exists('area',  $nonEmptyArray)) {
                $value = $nonEmptyArray['area'];
                $query = $query->where('clients.area', 'LIKE %', $value . '%');
            }
            if (array_key_exists('bank',  $nonEmptyArray)) {
                $value = $nonEmptyArray['bank'];
                $value = explode(',', $value);
                $query = $query->whereIn('discounts.bank_id', $value);
            }
            if (array_key_exists('bank-name',  $nonEmptyArray)) {
                $value = $nonEmptyArray['bank-name'];
                $value = explode(',', $value);
                $query = $query->whereIn('banks.bank', $value);
            }
            if (array_key_exists('cuisine',  $nonEmptyArray)) {
                $value = $nonEmptyArray['cuisine'];
                $cuisineArray = explode(', ',  $value);
                $query = $query->join('cousines', 'cousines.client_id', '=',  'clients.clid');
                foreach ($cuisineArray as $key => $value) {
                    $query = $query->where("cousines.cousine",  'like',  "%$value%");
                }
            }
            if (array_key_exists('sort', $nonEmptyArray)){
                $sort=$nonEmptyArray['sort'];
                if (array_key_exists('order', $nonEmptyArray)) {
                    $order=$nonEmptyArray['order'];
                }
                if (empty($order)) {
                    $order = 'desc';
                }
                $query = $query->orderByRaw("CAST($sort AS DECIMAL(10, 5)) $order");
            }

            if ($request->city_id != null) {
                $value = $request->city_id;
                $query = $query->where('clients.cityid', '=',  $value);
            }

            $query = $query->groupBy('clients.clid');
            #Select only required columns from the resultset.
            $query = $query->select(
                'clients.clid'
                , 'clients.countryid'
                , 'clients.stateid'
                , 'clients.cityid'
                , 'clients.Restaurant_Name'
                , 'clients.contact'
                , 'clients.email'
                , 'clients.address'
                , 'clients.area'
                , 'clients.hd'
                , 'clients.dinein'
                , 'clients.svegnn'
                , 'clients.bar'
                , 'clients.st'
                , 'clients.aircond'
                , 'clients.cuisine'
                , 'clients.knownfor'
                , 'clients.order_sugest'
                , 'clients.payment'
                , 'clients.cost'
                , 'clients.latitude'
                , 'clients.longitude'
                , 'avgrating'
                , DB::raw("GROUP_CONCAT(CONCAT(CONCAT(banks.bank,  ' - '), discounts.discount) SEPARATOR ',  ') AS discounts")
            );
        }



        #Try executing the query string which was generated.
        try {
            #Paginate and append the original request to the result (so that state is maintained for next/prev pages)
            $result = $query->simplePaginate(15)->appends($nonEmptyArray);
        }
        catch (Exception $e) {
            return Restable::bad()->render();
        }

         return Restable::listing($result)->render();
    }


    public function autocomplete(Request $request)
    {
        try {
            $take = $request->take;
            $cityId = $request->city_id;
            if ($request->exists('cuisine')) {
                $query = $request->input('cuisine');
                $query = '%' . $query . '%';
                $results = City::findOrFail($cityId)->cuisine()->where('cousine',  'like',  $query)->select('cousine')->distinct()->get()->take($take);
                return Restable::listing($results)->render();
            } else if ($request->exists('area')) {
                DB::enableQueryLog();
                $query = $request->input('area');
                $query = '%' . $query . '%';
                $results = City::findOrFail($cityId)->clients()->where('area',  'like',  $query)->select('area')->distinct()->get()->take($take);
                return Restable::listing($results)->render();
            } else if ($request->exists('client_or_area')) {
                DB::enableQueryLog();
                $query = $request->input('client_or_area');
                $query = '%' . $query . '%';
                $results = City::findOrFail($cityId)->clients()->where(function ($queryPart) use ($query) { $queryPart->where('area',  'like',  '%'.$query.'%')->orWhere('Restaurant_Name',  'like',  $query);})->select('Restaurant_Name', 'area')->groupby('Restaurant_Name')->orderBy('Restaurant_Name')->take($take)->get();
                return Restable::listing($results)->render();
            } else if($request->exists('bank')){
                $query = $request->input('bank');
                $query = '%' . $query . '%';
                $results = City::findOrFail($cityId)->banks()->where('bank',  'like',  $query)->distinct()->get()->take($take);
                return Restable::listing($results)->render();
            }
            else {
                return Restable::bad()->render();
            }
        }
        catch (ModelNotFoundException $e) {
            return Restable::missing(Constants::RESOURCEDOESNOTEXIST)->render();
        }
    }
}