<?php namespace App\Http\Controllers;


use App\Http\Requests;
use App\Models\Client;
use App\Models\Favourite;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Teepluss\Restable\Facades\Restable;

class FavouriteController extends Controller {

	/**
	 * Get all favourited marked of a particular user
	 *
	 * @return Response
	 */
	public function favouriteList()
	{
		$user = Auth::user();
		$favouriteList = Favourite::where('uid','=',$user->uid)->with('Client')->get();
		return $favouriteList;
	}

    public function setUserBank(Request $request) {
        $user = Auth::user();
        $banks = $request->banks;
        if (!empty($banks)) {
            if (empty($user->bank)) {
                $user->bank = $banks;
            } else {
                $user->bank = $user->bank . $banks;
            }
            $user->save();
        }
    }
}
