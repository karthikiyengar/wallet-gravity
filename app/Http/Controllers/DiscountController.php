<?php namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\HappyHours;
use App\Models\LiveOffers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Teepluss\Restable\Facades\Restable;

class DiscountController extends Controller {


    /**
     * Happy hours is calculated on date and city basis. City will be provdied as input.
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function showHappyHours(Request $request)
    {       
        $user = Auth::user();
        $city = $user->cityid; 
        $date = Carbon::now()->format('Y-m-d');
        $time = Carbon::now()->format('h:m A');
        $query = HappyHours::where('sd','<=',Carbon::now()->format('Y-m-d'))->where('ed','>=',Carbon::now()->format('Y-m-d'))->where('city','=', $city)->with('clients')->get();
        return Restable::listing($query)->render();
        /* Time 
        where('SUBSTR(stym,1,5)','<=','10:20 AM')->where('etym','>=','10:30')->->get();
        */    
    }

    /**
     *
     * Live Offers is calculated on date and city basis. City will be provdied as input.
     * @param $id
     */
    public function showLiveOffers(Request $request)
    {
         $user = Auth::user();
        $city = $user->cityid; 
        $date = Carbon::now()->format('Y-m-d');
        $time = Carbon::now()->format('h:m A');
        $query = LiveOffers::where('sd','<=',Carbon::now()->format('Y-m-d'))->where('ed','>=',Carbon::now()->format('Y-m-d'))->where('city','=', $city)->with('clients')->get();
        return Restable::listing($query)->render();
    }
}
