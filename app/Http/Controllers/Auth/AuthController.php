<?php namespace App\Http\Controllers\Auth;

use App\Admin;
use App\Classes\Constants;
use App\Http\Controllers\Controller;
use App\Models\Security;
use App\User;
use App\Models\ResetPassword;
use Config;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use League\OAuth2\Client\Provider\Facebook;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Provider\Google;
use Teepluss\Restable\Facades\Restable;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

    protected $auth;


    /**
     * Handle a default login request to the application.
     * @param Request $request
     * @return mixed
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $email = $request->input('username');
        $password = $request->input('password');

        $user = User::where('email', $email)->first();
        if($user!=null){
            if($user->social_flag == 'gm'){
                return "Please login with Google+.";
            }
            if($user->social_flag == 'fb'){
                return "Please login with Facebook.";
            }
            if (password_verify($password, $user->pass)) {
                if($user->verify_flag == 1){
                    Auth::login($user);
                    $customClaims = array('uid' => $user->uid);
                    $token = JWTAuth::fromUser($user);
                    return Restable::listing(array('api-token' => $token, 'user' => $user))->render();
                }
                else{
                    return "Please check your inbox for confirmatiom mail.";
                }
            }
            else {
                return "Password incorrect.";
            }
        }
        else{
            return "User not yet registered.";
        }
    }


    /**
     * Generates tokens for facebook social login
     * @param Request $request
     * @return static
     */
    public function postLoginFacebook(Request $request)
    {
        $fbToken = new AccessToken(array('access_token' => $request->input('fb_token')));
        $provider =  new \League\OAuth2\Client\Provider\Facebook([
            'clientId'          => Config::get('services.facebook.client_id'),
            'clientSecret'      => Config::get('services.facebook.client_secret'),
            'redirectUri'       => 'http://localhost/callback',
            'graphApiVersion'   => 'v2.4',
        ]);
        $userDetails = $provider->getResourceOwner($fbToken);
        $fbUserName = $userDetails->getFirstName() . ' ' . $userDetails->getLastName();
        $fbEmail = $userDetails->getEmail();
        $fbImg = $userDetails->getPictureUrl();
        $user = User::where(array('name' => $fbUserName, 'pass' => '', 'social_flag' => 'fb'))->first();
        if (!empty($user)) {
            return $this->loginUser($user);
        } else {
            $user = new User;
            $user->name = $fbUserName;
            $user->email = $fbEmail;
            $user->pass = '';
            $user->uimg = $fbImg;
            $user->verify_flag = 1;
            $user->social_flag = 'fb';
            $user->save();
            return $this->loginUser($user);
        }
    }

    public function postLoginGplus(Request $request) {
        $gplusToken = new AccessToken(array('access_token' => $request->input('gplus_token')));
        $provider =  new Google([
            'clientId'      => '739707660431-qip4kfpjdtd2d93njhkeo4pq4cbcn6bv.apps.googleusercontent.com',
            'clientSecret'  => 'VPvtmfjEVjNCh_3YJAkHE9JQ',
            'redirectUri'   => 'http://localhost/callback',
            'scopes'        => ['PROFILE'],
        ]);
        $userDetails = $provider->getResourceOwner($gplusToken);
        $gplusUserName = $userDetails->getFirstName() . ' ' . $userDetails->getLastName();
        $gplusEmail = $userDetails->getEmail();
        $gplusImg = $userDetails->getAvatar();
        $user = User::where(array('name' => $gplusUserName, 'pass' => '', 'social_flag' => 'gm'))->first();
        if (!empty($user)) {
            return $this->loginUser($user);
        } else {
            $user = new User;
            $user->name = $gplusUserName;
            $user->email = $gplusEmail;
            $user->pass = '';
            $user->uimg = $gplusImg;
            $user->verify_flag = 1;
            $user->social_flag = 'gm';
            $user->save();
            return $this->loginUser($user);
        }
    }

    /**
     * Log in the user, generate a token and return a JSON Response
     * @param $user
     * @return mixed
     */
    public function loginUser($user) {
        Auth::login($user);
        $token = JWTAuth::fromUser($user);
        return Restable::listing(array('api-token' => $token, 'user' => $user))->render();
    }

    /**
     * Logs out the token which has been provided (Invalidates) and logs out user.
     * @param Request $request
     * @return mixed
     */
    public function getLogout(Request $request)
    {
        $this->middleware('jwt.auth');
        JWTAuth::parseToken()->invalidate();
        $this->auth->logout();
        return Restable::success(Constants::LOGGEDOUT)->render();
    }

    /*
     * Handle registration of users
     * @params Request $request
     * @return mixed
     */
    public function register(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');
        $email = $request->input('email');

        $user = User::where('email', $email)->first();
        #Check if the user has registered with same email-id
        if($user != null){
            #If user is already register with same email then return appropriate message
            //return Restable::ALREADYREGISTERED(Constants::ALREADYREGISTERED)->render();
            return "Email ID already been registered.";
        }
        else{
            $this->validate($request, [
                'email' => 'required',
                'username' => 'required',
                'password' => 'required',
            ]);

            $randCode = uniqid();
            $data = [
                'uniqueid' => $randCode,
                'username' => $username,
                'email' => $email
            ];
            Mail::send('emails.register',$data, function ($message) use ($username,$email)
            {
                $message->to($email, $username)
                    ->replyTo('BrewferRegistrationConfirmation@brewfer.com', 'Brewfer')
                    ->subject("Dear $username, confirm your email!");
            });

            #If not then register him/her
            $user = new User();
            $user->name = $username;
            $user->email = $email;
            $user->pass = password_hash($password, PASSWORD_DEFAULT);
            //$user->cityid = 1;
            #Temporay fix for direct email verfication
            $user->verify_flag = 0;

            $user->save();
            $security = new Security();
            $security->sid = $randCode;
            $security->uid = $user->uid;
            $security->email_id = $email;
            $security->save();
            #send a verfication mail to user

            #send appropriate message to user's app.
            return "Thank you for registering. Please check you inbox for confirmation mail.";
        }
    }

    public function forgotPassword(Request $request){
        #Get input from user.
        $email = $request->input('email');
        $this->validate($request, [
            'email' => 'required',
            ]);
        #Fetch user details from DB
        $user = User::where('email', $email)->first();
        #Validate user
        if($user!=null){
            #Check if user register using socail media site
            if($user->social_flag == 'gm' || $user->social_flag == 'fb'){
                return "You have already logged with Gmail or Facebook.";
            }
            $username = $user->name;
            $token = uniqid();
            #If not sent user password mail
            $data = [
                'username' => $username,
                'token' => $token,
                'email' => $email
            ];
            Mail::send('emails.forgot-password',$data, function ($message) use ($username,$email)
            {
                $message->to($email, $username)
                    ->replyTo('info@brewfer.com', 'Brewfer')
                    ->subject("Dear $username, Here is your Password for Brewfer.");
            });

            date_default_timezone_set("Asia/Kolkata");
            $date = date('y-m-d H:i:s');

            $forgotPassword = new ResetPassword();
            $forgotPassword->email_id = $email;
            $forgotPassword->token = $token;
            $forgotPassword->stime = $date;
            $forgotPassword->save();
            #End response.
            return "Thank you for you request, please check your inbox for reset password link.";
            
        }
        else{
            return "Unregistered User";
        }
    }

    public function setProfile(Request $request){
        $mobileNumber  = $request->input('mobileNumber');
        $aboutMe = $request->input('aboutMe');
        $name = $request->input('name');
        try {
            $user = Auth::user();
            $user = User::where('uid', $user->uid)->first();
            $user->phone = $mobileNumber;
            $user->about = $aboutMe;
            $user->name = $name;
            $user->save();
            return $user;
        }
        catch (Exception $e) {
            return Restable::bad()->render();
        }
    }

    public function getProfile(Request $request){
        try {
            $user = Auth::user();
            return $user;
        }
        catch (Exception $e) {
            return Restable::bad()->render();
        }
    }

    public function getSettings(Request $request) {
        return ['showLiveOffers' => false];
    }

    public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
	}

    public function adminLogin(Request $request) {
        $username = $request->input('username');
        $password = $request->input('password');

        if (empty($username) || empty($password)) {
            return 'Invalid Request';
        }

        $admin = Admin::where('auname',$username)
                    ->where('apass', $password)->first();

        if (!empty($admin)) {
            $payload = JWTFactory::sub($admin->aid)->make();
            $token = JWTAuth::encode($payload);
            return ['status' => 'success', 'data' => (string) $token, 'message' => 'Logged in successfully'];
        } else {
            return ['status' => 'error', 'message' => 'Invalid Credentials'];
        }


    }
}