<?php namespace App\Http\Controllers;

use App\Commands\ProcessData;
use App\Http\Requests;
use App\Models\BankClient;
use App\Models\City;
use App\Models\Client;
use App\Models\Cuisine;
use App\Models\Discount;
use App\Models\Loader;
use App\Models\Timings;
use Exception;
use Illuminate\Http\Request;
use Input;
use Queue;

class LoadingTaskController extends Controller {
    public $mandatoryFields, $bitFields, $bankList, $cuisineList;

    public function readFile(Request $request)
    {
        # Get Inputs
        $file = Input::file();
        $cityId = $request->input('city_id');
        if (empty($file) || empty($cityId)) {
            return ["status" => "error", "message" => "Invalid Form"];
        }
        $fileName = $request->input('file_name');
        Input::file('bulk_upload')->move(app_path() . '/DataFiles', $fileName);


        $enableValidation = $request->input('enableValidation');
        $enableValidation = (empty($enableValidation)) ? false : true;

        Queue::push(new ProcessData($fileName, $cityId, $enableValidation));
        return ["status" => "success", "message" => "Job has been queued"];
    }

    public function getStats($id=null) {
        if (empty($id)) {
            return Loader::orderBy('id', 'a')->get();
        } else {
            $loader = Loader::find($id);
            $loader->error_list = unserialize($loader->error_list);
            return $loader;
        }
    }

    public function getCity() {
        return City::orderBy('cyname')->get();
    }

    public function deleteBatch(Request $request) {
        $loaderId = $request->input('loader_id');
        try {
            $clientIdsToDelete = Client::where('loader_id', $loaderId)->lists('clid');
            Client::whereIn('clid', $clientIdsToDelete)->delete();
            Timings::whereIn('client_id', $clientIdsToDelete)->delete();
            BankClient::whereIn('client_id', $clientIdsToDelete)->delete();
            Cuisine::whereIn('client_id', $clientIdsToDelete)->delete();
            Discount::whereIn('client_id', $clientIdsToDelete)->delete();

            $log = Loader::find($loaderId);
            $log->status = 'Deleted';
            $log->message = 'Batch Deleted by User';
            $log->save();
            return ["status" => "success", "message" => "Records deleted successfully"];
        } catch (Exception $e) {
            return ["status" => "error", "message" => "Exception Occurred"];
        }
    }
}
