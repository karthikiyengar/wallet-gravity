<?php namespace App\Http\Controllers;

use App\Classes\Constants;
use App\Http\Requests;
use App\Models\Comment;
use App\Models\Review;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SebastianBergmann\GlobalState\RuntimeException;
use Teepluss\Restable\Facades\Restable;

class CommentController extends Controller {


	/**
	 * create comment for a review if that particular review exists
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $user = Auth::user();
        $reviewId = $request->input('review_id');
        try{
            Review::findOrFail($reviewId);
            $comment = new Comment;
            $comment->uid = $user->uid;
            $comment->reviewid = $reviewId;
            $comment->time1 = date('y-m-d H:i:s');
            $comment->comment = $request->input('comment');
            $comment->save();
            return Restable::listing($comment->with('user', 'likes')->where('uid', $user->uid)->where('reviewid', $reviewId)->where('comment', $request->input('comment'))->first())->render();
        } catch(ModelNotFoundException $e){
            return Restable::missing(Constants::RESOURCEDOESNOTEXIST)->render();
        }
	}

	/**
	 * Update the comment only if that comment belongs to particular user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update( Request $request)
	{
        try {
            $user = Auth::user();
            $commentId=$request->input('comment_id');
            $reviewId = $request->input('review_id');
            $comment = Comment::findOrFail($commentId);
            if ($comment->user()->first() == $user) {
                $comment->uid = $user->uid;
                $comment->reviewid = $request->input('review_id');
                $comment->time1 = Carbon::now();
                $comment->comment = $request->input('comment');
                $comment->save();
                return Restable::listing($comment->with('user', 'likes')->where('uid', $user->uid)->where('reviewid', $reviewId)->where('comment', $request->input('comment'))->first())->render();
            }
            else {
                throw new RuntimeException();
            }
        }
        catch (ModelNotFoundException $e) {
            return Restable::missing(Constants::RESOURCEDOESNOTEXIST)->render();
        }
        catch (RuntimeException $e) {
            return Restable::unauthorized(Constants::RESOURCEUNAUTHORIZED)->render();
        }
	}

	/**
	 * Remove the specified comment check if that comment exists.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
        try {
            $user = Auth::user();
            $id = $request->input('comment_id');
            $comment = Comment::findOrFail($id);
            if ($comment->user()->first() == $user) {
                $comment->delete();
                return Restable::deleted()->render();
            }
            else {
                return Restable::unauthorized(Constants::RESOURCEUNAUTHORIZED)->render();
            }
        }
        catch (ModelNotFoundException $e) {
            return Restable::missing(Constants::RESOURCEDOESNOTEXIST)->render();
        }
        catch (RuntimeException $e) {
            return Restable::unauthorized(Constants::RESOURCEUNAUTHORIZED)->render();
        }
	}

}
