<?php
namespace App\Http\Controllers;

use App\Classes\Constants;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\LikeForReview;
use App\Models\Review;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RuntimeException;
use Teepluss\Restable\Facades\Restable;

class LikeReviewController extends Controller
{

    /**
     * Create a like for a review if that particular review exists
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $user               = Auth::user();
        $reviewId           = $request->input('review_id');
        $likeReview         = LikeForReview::firstOrNew(array(
            'uid' => $user->uid,
            'reviewid' => $reviewId,
        ));
        try {
            Review::findOrFail($reviewId);
            $likeReview->status = $request->input('status');
            $likeReview->uid= $user->uid;
            $likeReview->time1 = Carbon::now();
            $likeReview->save();
            return Restable::created($likeReview)->render();
        }
        catch (RuntimeException $e) {
            return Restable::missing(Constants::RESOURCEDOESNOTEXIST)->render();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $user       = Auth::user();
            $likeReview = LikeForReview::findOrFail($id)->where('uid',$user->uid);
            $likeReview->delete();
            return Restable::deleted()->render();
        }
        catch (Exception $e) {
            return Restable::bad()->render();
        }
    }
}
