<?php namespace App\Http\Controllers;

use App\Classes\Constants;
use App\Http\Requests;
use App\Models\Client;
use App\Models\Review;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SebastianBergmann\GlobalState\RuntimeException;
use Teepluss\Restable\Facades\Restable;

class ReviewController extends Controller {


    /**
     *  Create a review for a client if that particular client exists
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
	{
        $user = Auth::user();
        $clientId = $request->input('client_id');

        $review         = Review::firstOrNew(array(
            'uid' => $user->uid,
            'client_id' => $clientId,
        ));

        try{
            Client::findOrFail($clientId);
            $review->client_id=$clientId;
            $review->status = 0 ;
            $review->review = $request->input('review');
            $review->uid = $user->uid;
            $review->timer = date('y-m-d H:i:s');
            $review->save();
            return Restable::listing($review->with('user', 'likes')->where('uid', $user->uid)->where('client_id',$clientId)->get())->render();
        } catch(Exception $e){
            return Restable::bad()->render();
        }

	}

    /**
     * Get the reviews for a particular restaurant (Client ID)
     *
     * @param Request $request
     * @return Response
     * @internal param int $id
     */
	public function show(Request $request)
	{
        $id = $request->input('review_id');
        try {
            $reviewDetails = Review::with('user','likes','comments','comments.user','comments.likes')->where('rid', $id)->get();
            return Restable::listing($reviewDetails)->render();
        }
        catch (Exception $e) {
            return Restable::bad()->render();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        try {
            $user = Auth::user();
            $review = Review::findOrFail($id)->where('uid', $user->uid);
            $review->delete();
            return Restable::deleted()->render();
        }
        catch (Exception $e) {
            return Restable::bad()->render();
        }
	}
}
