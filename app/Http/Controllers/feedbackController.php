<?php namespace App\Http\Controllers;

use App\Classes\Constants;
use App\Http\Controllers\Controller;
use App\Models\Security;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use League\OAuth2\Client\Provider\Google;
use League\OAuth2\Client\Token\AccessToken;
use Philo\Laravel5Facebook\Facades\Facebook;
use Teepluss\Restable\Facades\Restable;
use Tymon\JWTAuth\Facades\JWTAuth;

class feedbackController extends Controller {

	/**
	 * Get feedback from user and mail it to admin
	 *
	 * @param  Request $request
	 * @return Response msg
	 */
	public function feedback(Request $request)
	{
		$this->validate($request, [
            'title' => 'required',
            'msg' => 'required',
            ]);

		$title = $request->input('title');
		$message = $request->input('msg');

		$user = Auth::user();
		$email = $user->email;
		$username = $user->name;
		$data = [
                'username' => $username,
                'title' => $title,
                'msg' => $message,
                'email' => $email
            ];
		Mail::send('emails.feedback',$data, function ($message) use ($username,$email)
        {
            $message->to('info@brewfer.com', 'Brewfer');
            	$message->bcc('prasad9gone@gmail.com', 'Prasad');
            	$message->bcc('karthikeyan.iyengar@gmail.com', 'Karthik');
            	$message->bcc('balakrishnan017@gmail.com', 'Bala');
                $message->replyTo($email, $username);
                $message->subject("Dear Admin you got feedback from $username.");
        });
        return 'Thank you for your feedback.';
	}

}
