<?php
namespace App\Http\Controllers;

use App\Classes\Commons;
use App\Http\Requests;
use App\Models\Client;
use App\Models\Favourite;
use App\Models\Rating;
use App\Models\Timings;
use App\User;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Teepluss\Restable\Facades\Restable;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class ClientProfileController extends Controller
{

    /**
     * Get client details.
     * @param Request $request
     * @return mixed
     */
    public function show(Request $request)
    {
        try {
            try {
                $user = JWTAuth::toUser(JWTAuth::getToken());
            } catch (JWTException $e) {
                //ignore if no token
            }
            $clientId = $request->input('client_id');

            $searchResults['clientDetails'] = Client::with(array('banners' => function($query){  $query->where('status', '=', '1'); },'photos' => function($query){  $query->where('status', '=', '1'); },'menus' => function($query){  $query->where('status', '=', '1'); }, 'review', 'review.user', 'review.likes', 'discounts', 'discounts.bank')) -> where('clid', $clientId) -> get();

            if (!empty($user)) {
                $res                                              = Favourite::where('uid', $user->uid)->where('client_id', $clientId)->get()->count();
                $searchResults['clientDetails'][0]['isFavourite'] = ($res == 1) ? true : false;

                $rating                                      = Rating::where('uid', $user->uid)->where('client_id', $clientId)->get();
                $searchResults['clientDetails'][0]['rating'] = (count($rating) == 0 ? '0' : $rating[0]['rating']);
            }
            $searchResults['clientDetails'][0]['votes'] = Favourite::where('client_id', $clientId)->get()->count();



            $averageRating = DB::table('ratings')->select(DB::raw('round(avg(rating),1) as avgRating'))->where('client_id', '=', $clientId)->get();

            $searchResults['clientDetails'][0]['avgRating'] = $averageRating[0]->avgRating;

            $dayMap = array(
                "0" => "Sunday",
                "1" => "Monday",
                "2" => "Tuesday",
                "3" => "Wednesday",
                "4" => "Thursday",
                "5" => "Friday",
                "6" => "Saturday"
            );

            $day = $dayMap[Carbon::now()->dayOfWeek];

            $timings                                      = Timings::where('client_id', '=', $clientId)->select('sun as Sunday', 'mon as Monday', 'tue as Tuesday', 'wed as Wednesday', 'thu as Thursday', 'fri as Friday', 'sat as Saturday')->get();
            $searchResults['clientDetails'][0]['timings'] = $timings[0];
            $re                                           = "/(?:\\d*?:?\\d*(?:.PM|.AM|.Midnight)|\\d* Hours|Closed)/i";
            $str                                          = $timings[0][$day];
            //$str = '8 AM to 11 AM and 05:00 PM to 1:00 AM';
            preg_match_all($re, $str, $matches);
            $matches = $matches[0];
            if (strcasecmp($matches[0], "Closed") == 0) {
                $searchResults['clientDetails'][0]['openStatus'] = "Closed Today";
            } else if (strcasecmp($matches[0], "24 Hours") == 0) {
                $searchResults['clientDetails'][0]['openStatus'] = "Open";
            } else {
                $openFlag = false;
                $point    = Carbon::now();
                for ($i = 0; $i < count($matches); $i += 2) {
                    $startTime = Carbon::parse($matches[$i]);
                    $endTime   = Carbon::parse($matches[$i + 1]);
                    if ($endTime < $startTime) {
                        $endTime = $endTime->addDay();
                    }
                    $openFlag = ($point->gte($startTime) && $point->lte($endTime));
                    if ($openFlag) {
                        break;
                    }
                }
                $searchResults['clientDetails'][0]['openStatus'] = $openFlag ? "Open" : "Closed";

                if ($openFlag == false) {
                    for ($j = 0; $j < count($matches); $j += 2) {
                        if ($j == count($matches) - 2) {
                            $reopen                                         = Carbon::parse($matches[0]);
                            $searchResults['clientDetails'][0]['openLater'] = $reopen->diffForHumans($point, true);
                            break;
                        }
                        $endTime = Carbon::parse($matches[$j + 1]);
                        $reopen  = Carbon::parse($matches[$j + 2]);
                        if ($point->gte($endTime) && $point->lte($reopen)) {
                            $searchResults['clientDetails'][0]['openLater'] = $reopen->diffForHumans($point, true);
                            break;
                        }
                    }
                }
            }

            return $searchResults;
        }
        catch (Exception $e) {
            return $e;
            return Restable::bad()->render();
        }
    }
    public function storeRating(Request $request)
    {
        $ratingValue = $request->input('rating');
        $clientId    = $request->input('client_id');
        $user        = Auth::user();
        try {
            $rating         = Rating::firstOrNew(array(
                'client_id' => $clientId,
                'uid' => $user->uid
            ));
            $rating->rating = $ratingValue;
            $rating->save();
            return Restable::listing($rating)->render();
        }
        catch (Exception $e) {
            return Restable::bad()->render();
        }
    }

    public function storeFavourite(Request $request)
    {
        $favValue = $request->input('fav');
        $clientId = $request->input('client_id');
        $user     = Auth::user();
        if ($favValue == true) {
            $favourite = Favourite::firstOrNew(array(
                'client_id' => $clientId,
                'uid' => $user->uid
            ));
            $favourite->save();
            return Restable::listing($favourite)->render();
        } else {
            $favourite = Favourite::where('client_id', '=', $clientId)->where('uid', '=', $user->uid);
            $favourite->delete();
            return Restable::deleted()->render();
        }
    }
}
