<?php
namespace App\Commands;

use App\Commands\Command;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Models\Client;
use League\Flysystem\Exception;

class CalculateGeoCoords extends Command implements SelfHandling, ShouldBeQueued
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {

    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $cannotBeDetermined = array();
        try {
            $clients = Client::where('is_geo_processed', '=', '0')->where('latitude', '=', NUll)->where('longitude', '=', NULL)->get();
            $i       = 1;
            if ($clients->count() != 0) {
                foreach ($clients as $client) {
                    if ($i >= 10) {
                        sleep(3);
                        $i = 1;
                    } else {
                        $i++;
                        #Get GEO Coordinates
                        $prepAddr = str_replace(' ', '+', $client->address);
                        try{
                            $geocode  = @file_get_contents('http://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false');
                            if ($geocode === FALSE) {
                                throw new Exception();
                            }
                            #json decode googleAPI response
                            $output = json_decode($geocode);
                            #if status is OVER_QUERY_LIMIT then exit
                            if ($output->status == 'OVER_QUERY_LIMIT') {
                                #Check Error message and status from google api response
                                break;
                            } else {
                                #Store coordinates in client record
                                if ($output->status == 'ZERO_RESULTS' || $output->status == 'UNKNOWN_ERROR') {
                                    $client->is_geo_processed   = 1;
                                    //$cannotBeDetermined[] = $client->Restaurant_Name;
                                    continue;
                                } else {
                                    if ($output->status == 'OK') {
                                        $client->latitude   = $output->results[0]->geometry->location->lat;
                                        $client->longitude  = $output->results[0]->geometry->location->lng;
                                        $client->is_geo_processed = 1;
                                    } else {
                                        $client->is_geo_processed = 1;
                                    }
                                }
                            }
                        }catch(Exception $e){
                            $client->is_geo_processed   = 0;
                        }finally{
                            $client->save();
                        }
                    }
                }
            }
        }
        catch (Exception $e) {
            //$outputResponse['ExceptionErrorMessage'] = "Exception occurred, Please try again. ";
        }
    }
}