<?php namespace App\Commands;

use App\Classes\DataExtractor;
use App\Classes\DataValidator;
use App\Models\Bank;
use App\Models\BankClient;
use App\Models\City;
use App\Models\Client;
use App\Models\Cuisine;
use App\Models\Discount;
use App\Models\Loader;
use App\Models\Timings;
use DateTime;
use Illuminate\Database\QueryException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use TijsVerkoyen\CssToInlineStyles\Exception;

class ProcessData extends Command implements SelfHandling, ShouldBeQueued {

    use InteractsWithQueue, SerializesModels;

    private $enableValidation, $cityId, $fileName, $log;
    /**
     * Create a new command instance.
     *
     */
    public function __construct($fileName, $cityId, $enableValidation)
    {
        $this->fileName = $fileName;
        $this->enableValidation = $enableValidation;
        $this->cityId = $cityId;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $this->log = new Loader;

        /* Update Loader Status */
        $this->log->status = 'Processing';
        $this->log->file_name = $this->fileName;
        $this->log->city = City::find($this->cityId)->cyname;
        $this->log->save();

        $filePath = app_path() . '/DataFiles/';
        $timestamp = new DateTime();
        $timestamp = $timestamp->format('Y-m-d-H_i_s');
        $newFileName = str_replace('.', $timestamp . '.',$this->fileName);

        # Attaching timestamp to file
        File::move($filePath . $this->fileName, $filePath . $newFileName);

        $fileUri = $filePath . $newFileName;

        # Init Validator
        $dataValidator = new DataValidator();

        # Init vars
        $errorList = array();
        $rowsToInsert = array();
        $rowNumber = 1;
        $isValidFile = false;

        Excel::selectSheetsByIndex(0)->filter('chunk')->load($fileUri)->chunk(100, function($results) use(&$rowsToInsert, &$errorList, &$rowNumber, $dataValidator, &$isValidFile)
        {
            if (!$isValidFile) {
                $result = $dataValidator->validateHeadersForRow($results->first());
                if (isset($result["success"])) {
                    $isValidFile = true;
                } else {
                    $this->log->status = 'Failed';
                    $this->log->message = 'Column format does not match. Please use file template provided.';
                    $this->log->error_list = serialize($result);
                    $this->log->save();
                    $this->delete();
                    die();
                }
            }
            foreach($results as $row)
            {
                # Row number incremented manually
                $rowNumber += 1;

                # For Validation Rules
                if ($this->enableValidation) {
                    # Pass row to validator and store errors returned in $result
                    $errors = $dataValidator->applyValidationToRow($row);
                    $result = [ 'Row ' . $rowNumber => $errors];

                    # Save rows which passed validation in $rowsToInsert and errors in $errorList
                    if (count($errors) == 0) {
                        $rowsToInsert[] = $row;
                    } else {
                        $errorList = array_merge($errorList, $result);
                    }
                }
                else {
                    $rowsToInsert[] = $row;
                }
            }
        });

        # We're done with Excel and Validation
        if(count($errorList) > 0 && $this->enableValidation) {
            # If there are any errors, return validation errors
            $this->log->status = 'Failed';
            $this->log->message = 'Validation Errors Detected';
            $this->log->number_of_records = $rowNumber;
            $this->log->records_with_errors = count($errorList);
            $this->log->error_list = serialize($errorList);
            $this->log->save();
            $this->delete();
        } else {
            # Begin Data Insertion
            $status = $this->dumpDataToTables($rowsToInsert);
        }
    }

    private function nullIfEmpty($string) {
        if (empty(trim($string))) {
            return null;
        } else {
            return trim($string);
        }
    }

    public function dumpDataToTables($arrayToDump)
    {
        try {
            $this->log->status = "Inserting Records";
            $this->log->records_inserted = 0;
            $this->log->number_of_records = count($arrayToDump);
            $this->log->save();
            $loaderId = $this->log->id;
            $dataExtractor = new DataExtractor();
            $banksToInsert = [];
            $cuisinesToInsert = [];
            foreach ($arrayToDump as $row) {
                $city = City::where('cyid', $this->cityId)->first();
                $stateId = $city->steid;
                $countryId = $city->cnid;

                $client = new Client;
                $client->cityid = $this->cityId;
                $client->countryid = $countryId;
                $client->stateid = $stateId;
                $client->Restaurant_Name = $this->nullIfEmpty($row->restaurant_name);
                $client->contact = $dataExtractor->extractPhoneNumbers($row->contact_numbers);
                $client->email = $this->nullIfEmpty($row->email);
                $client->address = $this->nullIfEmpty($row->address);
                $client->area = $this->nullIfEmpty($row->area);
                $client->hd = $dataExtractor->parseBoolean($row->home_delivery);
                $client->dinein = $dataExtractor->parseBoolean($row->dine_in);
                $client->svegnn = $dataExtractor->parseBoolean($row->veg_only);
                $client->bar = $dataExtractor->parseBoolean($row->bar);
                $client->aircond = $dataExtractor->parseBoolean($row->air_conditioned);
                $client->knownfor = $this->nullIfEmpty($row->known_for);
                $client->order_sugest = $this->nullIfEmpty($row->recommendations);
                $client->payment = $dataExtractor->extractPaymentData($row);
                $client->cost = $this->nullIfEmpty($row->cost_for_two);
                $client->cost_includes_alcohol = $dataExtractor->parseBoolean($row->cost_includes_alcohol);
                $client->loader_id = $loaderId;
                $client->mainid = 0;
                $client->submainid = 0;
                $client->timing = $this->nullIfEmpty($row->timings);
                $client->cuisine = $this->nullIfEmpty($row->cuisines);
                $client->bank = $this->nullIfEmpty($row->bank);
                $client->save();


                # Get client ID
                $clientId = $client->clid;

                $offers = $dataExtractor->extractBankData($row->offers);

                foreach ($offers as $bank => $offer) {

                    # Get Bank ID or create new if doesn't exist

                    $bankId = Bank::where('bank', $bank)->where('cityid', $this->cityId)->select('bank_id')->first();
                    if (empty($bankId)) {
                        $bankModel = new Bank;
                        $bankModel->bank = $bank;
                        $bankModel->cityid = $this->cityId;
                        $bankModel->status = 1;
                        $bankModel->save();
                        $bankId = $bankModel->bank_id;
                    } else {
                        $bankId = $bankId->bank_id;
                    }

                    # Insert into Discounts
                    $discount = new Discount;
                    $discount->bank_id = $bankId;
                    $discount->client_id = $clientId;
                    $discount->discount = $offer;
                    $discount->save();

                    # Insert into Bank Client (Legacy)
                    $bankClient = new BankClient;
                    $bankClient->bank_id = $bankId;
                    $bankClient->client_id = $clientId;
                    $bankClient->save();
                }

                # Insert timing data

                $timingModel = new Timings;
                $timingModel->client_id = $clientId;
                $timingModel->cityid = $this->cityId;
                $timingData = $dataExtractor->extractTimingData($row->timings);
                foreach ($timingData as $day => $time) {
                    $timingModel->$day = $time;
                };
                $timingModel->save();

                # Insert cuisine data

                $cuisineData = $dataExtractor->extractCuisineData($row->cuisines);
                foreach ($cuisineData as $cuisine) {
                    $cuisineModel = new Cuisine;
                    $cuisineModel->client_id = $clientId;
                    $cuisineModel->cousine = $cuisine;

                    # For maintaining legacy
                    $cuisineModel->cityid = $this->cityId;
                    $cuisineModel->save();
                }
                $this->log->records_inserted = ($this->log->records_inserted + 1);
                $this->log->save();
            }
        } catch (QueryException $e) {
            $this->logException($e);
            $this->delete();
            throw new Exception($e);
        } catch (Exception $e) {
            $this->logException($e);
            $this->delete();
            throw new Exception($e);
        }

        $this->log->status = 'Completed';
        $this->log->message = 'Records inserted successfully';
        $this->log->save();

        $this->delete();
    }

    public function logException($e) {
        $this->log->status = 'Failed';
        $this->log->error_list = serialize(["message" => $e->getMessage(), "stacktrace" => $e->getTraceAsString()]);
        $this->log->message = 'Exception Occured';
        $this->log->save();
    }
}
