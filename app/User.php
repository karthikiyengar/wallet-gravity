<?php namespace App;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthenticatableContract {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'userregister';
    protected $primaryKey = 'uid';
    protected $hidden = array('pass');
    public $timestamps = false;
    /*
     * TODO: You can also use the $hidden variable of the model to exclude certain fields from the JSON of the model
     */
    public function reviews() {
        return $this->hasMany('App\Models\Review', 'uid');
    }

    public function likereviews() {
        return $this->hasMany('App\Models\LikeForReview', 'uid');
    }

    public function likecomments() {
        return $this->hasMany('App\Models\LikeForComment', 'uid');
    }

    public function ratings() {
        return $this->hasMany('App\Models\Rating', 'uid');
    }

    public function favourites(){
        return $this->hasMany('App\Models\Favourite', 'uid');
    }

    public function city() {
        return $this->belongsTo('App\Models\City', 'cityid');
    }
    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->uid;
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->pass;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return null;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        return null;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return null;
    }
}
