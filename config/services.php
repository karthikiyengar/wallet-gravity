<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],
    'facebook' => [
        'client_id' => '366824603511939',
        'client_secret' => 'c7eab1df70c4e90d54a39f8b2e80abf3',
    ],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'key' => '',
		'secret' => '',
	],

	'sms' => [
		'url' => 'http://www.k3digitalmedia.in/sendsms/bulksms.php',
		'username' => 'k3wltgvt',
		'password' => '123456',
		'sender' => 'BREWFR',
	]
];
