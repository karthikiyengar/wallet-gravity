<?php

return [
    'mandatoryFields' => ['restaurant_name','address','area','cuisines'],
    'bitFields'  => ['home_delivery','dine_in','veg_only','bar','air_conditioned','accepts_cards','accepts_cash','accepts_meal_coupons','cost_includes_alcohol'],
    'numericFields' => ['cost_for_two', 'latitude', 'longitude'],
    'fileHeaders' => ['restaurant_name','contact_numbers','email','address','area','latitude','longitude','timings','home_delivery','dine_in','veg_only','bar','air_conditioned','cuisines','known_for','recommendations','accepts_cash','accepts_cards','accepts_meal_coupons','cost_for_two','cost_includes_alcohol','offers']
];