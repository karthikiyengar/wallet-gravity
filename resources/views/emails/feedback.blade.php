<?php
/**
 * Created by PhpStorm.
 * User: prasad
 * Date: 6/4/2015
 * Time: 12:13 AM
 */
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
         <title>Brewfer Feedback</title>
         <style type="text/css">
         a{ list-style:none; color:#333; text-decoration:none;}
         a:hover{ color:#00F;}

         .confirm{width:200px; height:30px;padding:2px; background:#900; color:#FFF; text-align:center; font-weight:bold; margin:auto; line-height:30px; border-radius:10px;}
         .confirm a{ color:#FFF;}
         .confirm a:hover{ color:#00F;}
         </style>
    </head>
<body>
 <table width="100%" align="center" cellpadding="5"  bgcolor="#4a3e4a">
 <tr>
   <td>&nbsp;</td>
 </tr>
 <tr>
 <td>
 <table width="90%" align="center" cellpadding="2" cellspacing="2" bgcolor="#FFF" style="padding:20px;">
 <tr>
 <td align="center"><p align="center" style="font-family:Helvetica, Arial, sans-serif; color:#666; font-size:15px;"><strong>Feedback from:  {{ ucwords($username) }},</strong></p></td>
 </tr>
 <tr>
   <td align="center" style=" font-family: Helvetica, Arial, sans-serif; font-size:21px; color:#900; padding:5px;"><!--<img src="http://scsp.in/walled-emailer/images/pass-f.png" />--><strong>
   Title:</strong></td></tr>
 <tr>
   <td align="center" style=" font-family: Helvetica, Arial, sans-serif; font-size:21px; color:#FF0A00; padding:5px;" ><strong>{{ $title }}</strong></td>
 </tr>
 <tr>
   <td align="center" ><div  style="width:200px; font-family: Helvetica, Arial, sans-serif; height:30px;padding:2px; background:#87000c; color:#333; text-align:center; font-weight:bold; margin:auto; line-height:30px; border-radius:10px;">Feedback</div></td>
 </tr>
 <tr>
   <td style="font-family:Helvetica, Arial, sans-serif; color:#333; font-size:16px; text-align:center" >{{ $msg }}</td>
 </tr>
 <tr>
   <td align="center" ></td>
 </tr>
 <!-- <tr>
   <td align="center" style="font-family:Verdana, Geneva, sans-serif; color:#333; font-size:14px;" ><span style="font-family:; color:#333; font-size:25px;"><img src="http://brewfer.com/images/mail-images/give-r.png" /></span></td>
 </tr> -->
 <tr>
   <td align="center" ><img src="http://www.brewfer.com/images/wallet-gravity2.png" width="192" height="90" /></td>
 </tr>
  <tr>
   <td align="center" >

   	<table cellpadding="5" cellspacing="5">
     <!-- <tr>
     	<td style=" font-family:Helvetica, Arial, sans-serif;"><a href="#"><img src="http://brewfer.com/images/mail-images/pintrest.jpg" width="34" height="34"></a></td>
         <td style="font-family:Helvetica, Arial, sans-serif; color:#333; font-size:14px;"><a href="#" style="color:#333;text-decoration:none;"><strong>Pinterest</strong></a></td>
         <td><a href="#"><img src="http://brewfer.com/images/mail-images/facebook.jpg" alt="" width="34" height="34"></a></td>
         <td style="font-family:Helvetica, Arial, sans-serif; color:#333; font-size:14px;"><a href="#" style="color:#333;text-decoration:none;"><strong>Facebook</strong></a></td>
         <td><a href="#"><img src="http://brewfer.com/images/mail-images/twitter.jpg" width="34" height="34"></a></td>
         <td style="font-family:Helvetica, Arial, sans-serif; color:#333; font-size:14px;"><a href="#" style="color:#333;text-decoration:none;"><strong>Twitter</strong></a></td>
         <td><a href="#"><img src="http://brewfer.com/images/mail-images/google-plus.jpg" alt="" width="34" height="34"></a></td>
         <td style="font-family:Helvetica, Arial, sans-serif; color:#333; font-size:14px;"><a href="#" style="color:#333;text-decoration:none;"><strong>Google</strong></a></td>
         <td><a href="#"><img src="http://brewfer.com/images/mail-images/blogs.jpg" alt="" width="34" height="34"></a></td>
         <td style="font-family:Helvetica, Arial, sans-serif; color:#333; font-size:14px;"><a href="#" style="color:#333;text-decoration:none;"><strong>Blog</strong></a></td>
     </tr> -->
     </table>


   </td>
 </tr>
 </table>
 </td>
 </tr>
 <tr bgcolor="#4a3e4a">
   <td align="center"  style="color:#786878;">---------------------------------------------</td>
 </tr>
 <tr >
   <td style="color:#FFF;"><p align="center" style="font-family:Helvetica, Arial, sans-serif; color:#FFF; font-size:12px;">This email was sent to&nbsp;<a href="{{ $email }}" target="_blank" style="color:#FFF;text-decoration:none;">{{ $email }}</a>.&nbsp;<br />
     Dont want to receive this type of email?&nbsp;<strong>Unsubscribe</strong>.&nbsp;<br />
     Have a question?&nbsp;<strong>Visit our Help Center.</strong><br />
     &copy; BREWFER.&nbsp;<br />
     20/40, West Patel Nagar,&nbsp;<br />
     New Delhi, 110008.&nbsp;<br />
     <strong>Privacy Policy</strong>&nbsp;|&nbsp;<strong>Terms and Conditions</strong></p></td>
 </tr>
 <tr bgcolor="#4a3e4a">
   <td>&nbsp;</td>
 </tr>
 </table>

 </body>
</html>