<html>
   <head>
      <title>
        Brewfer Reset Password Request
      </title>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
      <style>
         body{
         font-family: 'Lato', sans-serif;
         padding:0px;
         margin:0px;
         }
      </style>
   </head>
   <body>
      <table width="650" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; border:1px solid #b1b1b1">
         <tr>
            <td><img src="http://brewfer.com/images/top.jpg"></td>
         </tr>
         <tr>
            <td>
               <p style="text-align:center; color:#760053; padding:30px 0 10px; font-family:Lato; font-size:18px;"><strong>Hi</strong> {{ ucwords($username) }},
               <p>
            </td>
         </tr>
         <tr>
            <td>
               <p style="text-align:center; padding:20px; font-size:16px; color:#1d1d1b">Click on the following link to create a new password for your brewfer.com account</p>
            </td>
         </tr>
         <tr>
            <td align="center"><a href="http://brewfer.com/reset-password.php?id={{ $token  }}"><img src="http://brewfer.com/images/creat_password.png" style="margin:10px 0 20px;"></a></td>
         </tr>
         <tr>
            <td>
               <hr style="margin:20px">
            </td>
         </tr>
         <tr>
            <td>
               <p style="text-align:center;font-size:15px;padding-bottom:10px; color:#1d1d1b; line-height:25px;">For any query or clarification, please feel free to contact us and we will be glad to assist you.<br>
                  <a href="#" style="color:#005c76;text-decoration:none;">admin@brewfer.com</a>
               </p>
            </td>
         </tr>
      </table>
   </body>
</html>