<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('loaders');
        Schema::create('loaders', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->integer('number_of_records')->nullable();
            $table->integer('records_inserted')->nullable();
            $table->integer('records_with_errors')->nullable();
            $table->mediumText('error_list')->nullable();
            $table->string('file_name');
            $table->string('city');
            $table->string('message')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loaders');
    }
}
