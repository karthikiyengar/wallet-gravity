<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('clients', function($table)
        {
            // Alter statements
            DB::statement("ALTER TABLE `clients` MODIFY `knownfor` varchar(255) null");
            DB::statement("ALTER TABLE `clients` MODIFY `order_sugest` varchar(255) null");
            DB::statement("ALTER TABLE `clients` MODIFY `bank` varchar(255) null");
            DB::statement("ALTER TABLE `clients` MODIFY `cost` varchar(50) null");
            DB::statement("ALTER TABLE `clients` MODIFY `email` varchar(50) null");
            DB::statement("ALTER TABLE `clients` MODIFY `contact` varchar(50) null");

            // New columns
            if (!Schema::hasColumn('clients', 'is_geo_processed')) {
                $table->boolean('is_geo_processed');
            }
            if (!Schema::hasColumn('clients', 'cost_includes_alcohol')) {
                $table->boolean('cost_includes_alcohol')->nullable();
            }
            if (!Schema::hasColumn('clients', 'latitude')) {
                $table->float('latitude');
            }
            if (!Schema::hasColumn('clients', 'longitude')) {
                $table->float('longitude');
            }
            if (!Schema::hasColumn('clients', 'loader_id')) {
                $table->int('loader_id');
            }
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('clients', function($table) {
            // Alter statements
            $table->dropColumn(['longitude', 'latitude', 'loader_id', 'is_geo_processed','cost_includes_alcohol']);
        });
    }

}
