<?php

use App\Classes\DataExtractor;

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$response = $this->call('GET', '/');
		$this->assertEquals(200, $response->getStatusCode());
	}

    public function testGetOpenClosed() {
        $dataExtractor = new DataExtractor();
        var_dump('11 AM to 1 PM');
        $this->assertTrue($dataExtractor->checkIfValidTimePeriod('11 AM to 1 PM'));
        echo '2 PM to 1 PM';
        $this->assertTrue($dataExtractor->checkIfValidTimePeriod('2 PM to 1 PM'));
    }

}
